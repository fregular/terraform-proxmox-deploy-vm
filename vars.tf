variable "cores" {
  description = "Number of cores of the vm"
  default     = 2
}

variable "sockets" {
  description = "Number of sockets of the vm"
  default     = 1
}


variable "memory" {
  description = "memory in mb"
  default     = 2048
}

variable "disk" {
  description = "Size of disk in GB, example '20G'"
  default     = "10G"
}

variable "pam_api_url" {
  description = "The url from Proxmox API"
}

variable "pam_user" {
  description = "User proxmox"
}

variable "pam_password" {
  description = "Password from proxmox user"
}

variable "count_vm" {
  description = "count of vm to create"
}

variable "ciuser" {
  description = "user cloud init"
}

variable "clone" {
  description = "image to clone"
  default     = "centos7.9"
}

variable "ssh_key" {
}

variable "hostname" {
}

variable "environment" {
  description = "environment of vm"
  default     = "qa"
}

variable "offset" {
  description = "last octet of ipaddress"
}

