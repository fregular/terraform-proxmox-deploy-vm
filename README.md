Para utilizar el proyecto, debe crear un terraform.tfvars y definir las variables:

* cores = 2
* sockets = 1
* memory = 2048
* disk = "30G"
* pam_api_url = "https://proxmox_server:8006/api2/json"
* pam_user = "root@pam"
* pam_password = "password_root_proxmox"
* count_vm = 1   #cantidad de servidores a crear
* clone = "centos7.9" #imagen template
* ssh_key = "llave ssh publica"
* ciuser = "usuario usado en cloudinit"
* hostname = "kube-terraform" 
* environment= "prod"
* offset = "181" 

El hostname y environment se concatenan para crear el hostname definitivo. El offset se agrega el ultimo octeto de la ipv4 que tendra la vm, si el count_vm es = 2, entonces se asignara la ip 181 y 182.
La red se encuentra hardcodeada en main.tf

para descargar el provider proxmox:

```
terraform init
```

Ejecute el plan:

```
terraform plan
```

si esta de acuerdo con todo:

```
terraform apply
```


Si quiere destruir:

```
terraform destroy
```
..
