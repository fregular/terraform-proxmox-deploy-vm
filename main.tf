resource "proxmox_vm_qemu" "proxmox_vm" {
  ciuser = var.ciuser
  count  = var.count_vm
  name        = join("-", ["${var.hostname}","${var.environment}","${count.index}"])
  target_node = "pve"
  clone       = var.clone
  os_type     = "cloud-init"
  cores       = var.cores
  sockets     = var.sockets
  cpu         = "host"
  memory      = var.memory
  scsihw      = "virtio-scsi-pci"
  bootdisk    = "scsi0"
  agent       = 1
  disk {
    size     = var.disk
    type     = "scsi"
    storage  = "local-vm_data"
    iothread = 1
  }
  network {
    model  = "virtio"
    bridge = "vmbr0"
  }
  lifecycle {
    ignore_changes = [
      tags,
    ]
    #    prevent_destroy = true
  }
  # Cloud Init Settings
  ipconfig0  = "ip=192.168.122.${var.offset + count.index}/24,gw=192.168.122.1"
  nameserver = "8.8.8.8"
  sshkeys    = <<EOF
  ${var.ssh_key}
  EOF

}


