terraform {
  required_providers {
    proxmox = {
      source = "telmate/proxmox"
    }
  }
}


provider "proxmox" {
  # Configuration options
  pm_api_url      = var.pam_api_url
  pm_user         = var.pam_user
  pm_password     = var.pam_password
  pm_tls_insecure = "true"
}

